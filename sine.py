#/////////////////////////////////////////////////////////////////////
#//
#// Crude Sinewave LUT generator
#//
#// BSD 2-Clause License
#//
#// Copyright (c) 2018, Jari Tulilahti
#// All rights reserved.
#//
#// See LICENSE for more information
#//
#/////////////////////////////////////////////////////////////////////

from __future__ import print_function
import math


print("const uint16_t sine[733] = {", end="\n    // 50 Hz\n    ")

# 50Hz
deg = 0.0
count = 0
freq = 499
increment = 180.0 / 399.99

while deg < 180:
    pwmval = 255 - int(math.sin(math.radians(deg)) * 255)
    count += 1
    if count == 10:
        print("0x%03X," % pwmval, end="\n    ")
        count = 0
    else:
        print("0x%03X, " % pwmval, end="")
    deg += increment

print("// 60 Hz", end="\n    ")

# 60Hz
deg = 0.0
count = 0
freq = 499
increment = 180.0 / 332.99

while deg < 180:
    pwmval = int(math.sin(math.radians(deg)) * freq)
    count += 1
    if count == 10:
        print("0x%03X," % pwmval, end="\n    ")
        count = 0
    else:
        print("0x%03X, " % pwmval, end="")
    deg += increment


print("\n};", end="\n\n")
