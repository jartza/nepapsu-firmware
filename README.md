NepaPSU
=======

PSU for Commodore 64.

Firmware is pretty simple complementary PWM power stage and Sine PWM driver.

Hardware schematics & board layout: https://gitlab.com/jartza/nepapsu/

Firmware for Attiny416 (during development, Attiny817). Compile with avr-gcc 8.2.0.
