///////////////////////////////////////
//                                   //
// NepaPSU firmware for Attiny416    //
//                                   //
// BSD 2-Clause License              //
//                                   //
// Copyright (c) 2018 Jari Tulilahti //
// All rights reserved.              //
//                                   //
// See LICENSE for more information  //
//                                   //
///////////////////////////////////////
#include <avr/io.h>
#include <avr/interrupt.h>

// Configurable values
#define PWM_FREQ 280000
#define VOL_MIN 11.0
#define VOL_MAX 11.2

// Power stage PWM & NPWM value macros
#define PWM_PERIOD (((F_CPU / PWM_FREQ) >> 1) - 1)
#define PWM_RES_UPPER (PWM_PERIOD << 6)

// SPWM frequency = 40kHz
#define SPWM_PERIOD 499
#define SPWM_50_COUNT 400

// Voltage monitor calculation macros, already simplified to 
// take into account the voltage divider (10/33), 8 times
// ADC accumulation and reference voltage (4.3V)
#define ADC_WINMIN (uint16_t)(VOL_MIN * 458)
#define ADC_WINMAX (uint16_t)(VOL_MAX * 458)

// 0 = 50Hz, 1 = 60Hz. TODO: Make jumper selector for this
volatile uint8_t out_frequency = 0;

// Delay SPWM starting until we have hit the upper limit of
// voltage window.
volatile uint8_t spwm_delay_start = 1;

// Lookup table for Sine (only 50Hz now, TODO: 60Hz). SPWM carrier
// frequency is 40kHz.
const uint16_t sine50[400] = {
    0x000, 0x003, 0x007, 0x00B, 0x00F, 0x013, 0x017, 0x01B, 0x01F, 0x023,
    0x027, 0x02B, 0x02E, 0x032, 0x036, 0x03A, 0x03E, 0x042, 0x046, 0x04A,
    0x04E, 0x051, 0x055, 0x059, 0x05D, 0x061, 0x065, 0x069, 0x06C, 0x070,
    0x074, 0x078, 0x07C, 0x07F, 0x083, 0x087, 0x08B, 0x08E, 0x092, 0x096,
    0x09A, 0x09D, 0x0A1, 0x0A5, 0x0A9, 0x0AC, 0x0B0, 0x0B4, 0x0B7, 0x0BB,
    0x0BE, 0x0C2, 0x0C6, 0x0C9, 0x0CD, 0x0D0, 0x0D4, 0x0D8, 0x0DB, 0x0DF,
    0x0E2, 0x0E6, 0x0E9, 0x0EC, 0x0F0, 0x0F3, 0x0F7, 0x0FA, 0x0FE, 0x101,
    0x104, 0x108, 0x10B, 0x10E, 0x111, 0x115, 0x118, 0x11B, 0x11E, 0x122,
    0x125, 0x128, 0x12B, 0x12E, 0x131, 0x134, 0x138, 0x13B, 0x13E, 0x141,
    0x144, 0x147, 0x14A, 0x14C, 0x14F, 0x152, 0x155, 0x158, 0x15B, 0x15E,
    0x160, 0x163, 0x166, 0x169, 0x16B, 0x16E, 0x171, 0x173, 0x176, 0x178,
    0x17B, 0x17D, 0x180, 0x182, 0x185, 0x187, 0x18A, 0x18C, 0x18F, 0x191,
    0x193, 0x195, 0x198, 0x19A, 0x19C, 0x19E, 0x1A1, 0x1A3, 0x1A5, 0x1A7,
    0x1A9, 0x1AB, 0x1AD, 0x1AF, 0x1B1, 0x1B3, 0x1B5, 0x1B7, 0x1B9, 0x1BA,
    0x1BC, 0x1BE, 0x1C0, 0x1C1, 0x1C3, 0x1C5, 0x1C6, 0x1C8, 0x1C9, 0x1CB,
    0x1CD, 0x1CE, 0x1CF, 0x1D1, 0x1D2, 0x1D4, 0x1D5, 0x1D6, 0x1D8, 0x1D9,
    0x1DA, 0x1DB, 0x1DC, 0x1DE, 0x1DF, 0x1E0, 0x1E1, 0x1E2, 0x1E3, 0x1E4,
    0x1E5, 0x1E6, 0x1E6, 0x1E7, 0x1E8, 0x1E9, 0x1EA, 0x1EA, 0x1EB, 0x1EC,
    0x1EC, 0x1ED, 0x1EE, 0x1EE, 0x1EF, 0x1EF, 0x1EF, 0x1F0, 0x1F0, 0x1F1,
    0x1F1, 0x1F1, 0x1F2, 0x1F2, 0x1F2, 0x1F2, 0x1F2, 0x1F2, 0x1F2, 0x1F2,
    0x1F2, 0x1F2, 0x1F2, 0x1F2, 0x1F2, 0x1F2, 0x1F2, 0x1F2, 0x1F2, 0x1F1,
    0x1F1, 0x1F1, 0x1F0, 0x1F0, 0x1EF, 0x1EF, 0x1EF, 0x1EE, 0x1EE, 0x1ED,
    0x1EC, 0x1EC, 0x1EB, 0x1EA, 0x1EA, 0x1E9, 0x1E8, 0x1E7, 0x1E6, 0x1E6,
    0x1E5, 0x1E4, 0x1E3, 0x1E2, 0x1E1, 0x1E0, 0x1DF, 0x1DE, 0x1DC, 0x1DB,
    0x1DA, 0x1D9, 0x1D8, 0x1D6, 0x1D5, 0x1D4, 0x1D2, 0x1D1, 0x1CF, 0x1CE,
    0x1CD, 0x1CB, 0x1C9, 0x1C8, 0x1C6, 0x1C5, 0x1C3, 0x1C1, 0x1C0, 0x1BE,
    0x1BC, 0x1BA, 0x1B8, 0x1B7, 0x1B5, 0x1B3, 0x1B1, 0x1AF, 0x1AD, 0x1AB,
    0x1A9, 0x1A7, 0x1A5, 0x1A3, 0x1A1, 0x19E, 0x19C, 0x19A, 0x198, 0x195,
    0x193, 0x191, 0x18F, 0x18C, 0x18A, 0x187, 0x185, 0x182, 0x180, 0x17D,
    0x17B, 0x178, 0x176, 0x173, 0x171, 0x16E, 0x16B, 0x169, 0x166, 0x163,
    0x160, 0x15E, 0x15B, 0x158, 0x155, 0x152, 0x14F, 0x14C, 0x149, 0x147,
    0x144, 0x141, 0x13E, 0x13B, 0x137, 0x134, 0x131, 0x12E, 0x12B, 0x128,
    0x125, 0x122, 0x11E, 0x11B, 0x118, 0x115, 0x111, 0x10E, 0x10B, 0x108,
    0x104, 0x101, 0x0FD, 0x0FA, 0x0F7, 0x0F3, 0x0F0, 0x0EC, 0x0E9, 0x0E5,
    0x0E2, 0x0DF, 0x0DB, 0x0D7, 0x0D4, 0x0D0, 0x0CD, 0x0C9, 0x0C6, 0x0C2,
    0x0BE, 0x0BB, 0x0B7, 0x0B4, 0x0B0, 0x0AC, 0x0A8, 0x0A5, 0x0A1, 0x09D,
    0x09A, 0x096, 0x092, 0x08E, 0x08B, 0x087, 0x083, 0x07F, 0x07C, 0x078,
    0x074, 0x070, 0x06C, 0x068, 0x065, 0x061, 0x05D, 0x059, 0x055, 0x051,
    0x04E, 0x04A, 0x046, 0x042, 0x03E, 0x03A, 0x036, 0x032, 0x02E, 0x02B,
    0x027, 0x023, 0x01F, 0x01B, 0x017, 0x013, 0x00F, 0x00B, 0x007, 0x003,
};  

void pwm_duty_set(uint16_t duty);

// Very simple SPWM generator using Timer A and LUT
ISR(TCA0_OVF_vect) {
    static uint16_t counter = 0;
    static uint8_t active_pwm = 0;

    // Read SPWM value from LUT
    uint16_t spwm_duty = sine50[counter++];
    if (counter == SPWM_50_COUNT) {
        active_pwm ^= 1;
        counter = 0;
    }

    // Set one of the compare register to duty
    // cycle and the other to zero, depending
    // which one is active
    if (active_pwm) {
        TCA0.SINGLE.CMP0BUF = spwm_duty;
        TCA0.SINGLE.CMP1BUF = 0;
    } else {
        TCA0.SINGLE.CMP0BUF = 0;
        TCA0.SINGLE.CMP1BUF = spwm_duty;
    }

    // Clear OVF interrupt
    TCA0.SINGLE.INTFLAGS = TCA_SINGLE_OVF_bm;
}

// ADC outside window interrupt
ISR(ADC0_WCOMP_vect) {
    static uint16_t pwm_duty = 0;

    uint16_t adc_value = ADC0.RES;

    if (adc_value < ADC_WINMIN) {
        if (pwm_duty < PWM_PERIOD) {
            pwm_duty++;
        }
    } else {
        if (pwm_duty > 0) {
            // Enable SPWM if it's still in startup delay mode
            if (spwm_delay_start) {
                spwm_delay_start = 0;
            }
            pwm_duty--;
        }
    }

    pwm_duty_set(pwm_duty);

    // Clear WCOMP interrupt
    ADC0.INTFLAGS = ADC_WCMP_bm;
}

void clock_init(void) {
    // Use 20MHz clock (protected IO write)
    CPU_CCP = CCP_IOREG_gc;
    CLKCTRL.MCLKCTRLA = CLKCTRL_CLKOUT_bm;

    // Disable prescaler to run at full speed
    // (protected IO write)
    CPU_CCP = CCP_IOREG_gc;
    CLKCTRL.MCLKCTRLB = 0;
}

void adc_init() {
    // Set internal reference to 4.34V and force-enable
    // ADC reference voltage
    VREF.CTRLA = VREF_ADC0REFSEL_4V34_gc;
    VREF.CTRLB = VREF_ADC0REFEN_bm;

    // Using ADC0 in AIN9 (pin PB4), 8 accumulated samples,
    // interrupt when voltage goes outside selected window,
    // Prescaler /16 (1.25MHz), reduced sample capacitance
    ADC0.CTRLA = ADC_FREERUN_bm | ADC_ENABLE_bm;
    ADC0.CTRLB = ADC_SAMPNUM_ACC8_gc;
    ADC0.CTRLC = ADC_SAMPCAP_bm | ADC_PRESC_DIV16_gc;
    ADC0.CTRLE = ADC_WINCM_OUTSIDE_gc;
    ADC0.MUXPOS = ADC_MUXPOS_AIN9_gc;
    ADC0.INTCTRL = ADC_WCMP_bm;

    // Window min and max
    ADC0.WINLT = ADC_WINMIN;
    ADC0.WINHT = ADC_WINMAX;

    // Start first conversion
    ADC0.COMMAND = ADC_STCONV_bm;
}

void timer_a_init() {
    // PWM single slope mode, enable compare 0 and 1
    TCA0.SINGLE.CTRLB = TCA_SINGLE_WGMODE_SINGLESLOPE_gc | TCA_SINGLE_CMP0EN_bm | TCA_SINGLE_CMP1EN_bm;
    
    // Enable Overflow interrupt
    TCA0.SINGLE.INTCTRL = TCA_SINGLE_OVF_bm;
    
    // Set timer period
    TCA0.SINGLE.PER = SPWM_PERIOD;

    // Enable timer with default clock (20MHz int, div /1)
    TCA0.SINGLE.CTRLA = TCA_SINGLE_ENABLE_bm;
}

void pwm_duty_set(uint16_t duty) {
    static uint8_t stopped = 1;

    if (duty) {
        if (stopped) {
            // Restart timer if timer was stopped
            stopped = 0;
            TCD0.CTRLA = TCD_ENABLE_bm;
        }
        TCD0.CMPASET = PWM_PERIOD - duty;   // OUT A off time
        TCD0.CMPACLR = duty;                // OUT A on time
        TCD0.CMPBSET = PWM_PERIOD - duty;   // OUT B off time
        TCD0.CMPBCLR = duty;                // OUT B on time
        TCD0.CTRLE = TCD_SYNCEOC_bm;        // Synchronize
    } else {
        // Stop timer if duty cycle is set to 0
        TCD0.CTRLE = TCD_DISEOC_bm;
        stopped = 1;
    }
}

void timer_d_init() {
    // Four ramp mode
    TCD0.CTRLB = TCD_WGMODE_FOURRAMP_gc;

    // Sync buffer values at the end of cycle
    TCD0.CTRLE = TCD_SYNCEOC_bm;

    // Enable compare output pins (protected IO write)
    CPU_CCP = CCP_IOREG_gc;
    TCD0.FAULTCTRL = TCD_CMPAEN_bm | TCD_CMPBEN_bm;

    // Set initial duty to 0 at start. Timer gets enabled
    // first time duty cycle is set to over zero
    pwm_duty_set(0);
}

void pins_mux() {
    // Timer A compare output pins for SPWM
    PORTB.OUTCLR = PIN0_bm | PIN1_bm;
    VPORTB.DIR = PIN0_bm | PIN1_bm;

    // ADC pin (AIN9/PB4), set as input and disable digital input
    // This if used to regulate output voltage (12.7V)
    PORTB.DIRCLR = PIN4_bm;
    PORTB.PIN4CTRL = PORT_ISC_INPUT_DISABLE_gc;

    // Timer D compare output pins for PWM/NPWM
    PORTA.OUTCLR = PIN4_bm | PIN5_bm;
    VPORTA.DIR = PIN4_bm | PIN5_bm;
}

int main(void)
{
    pins_mux();
    clock_init();
    adc_init();
    timer_d_init();
    sei();

    // Wait for voltage to rise before enabling SPWM
    while (spwm_delay_start);
    timer_a_init();

    // Loop forever
    while(1);
}
