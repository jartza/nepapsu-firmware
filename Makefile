DEVICE     = attiny416
CLOCK      = 20000000
PROGRAMMER = atmelice_updi
OBJECTS    = main.o

# FUSES      = -B4 -U lfuse:w:0xe0:m -U hfuse:w:0xdf:m -U efuse:w:0xfe:m

AVRDUDE = avrdude -c $(PROGRAMMER) -p $(DEVICE) -V
COMPILE = avr-gcc -Wall -std=gnu99 -Os -DF_CPU=$(CLOCK) -mmcu=$(DEVICE) \
    -B atmel_pack/gcc/dev/$(DEVICE)/ -I atmel_pack/include -Wa,-mgcc-isr

# symbolic targets:
all: main.hex

.c.o:
	$(COMPILE) -c $< -o $@

.S.o:
	$(COMPILE) -x assembler-with-cpp -c $< -o $@

.c.s:
	$(COMPILE) -S $< -o $@

flash:	all
	$(AVRDUDE) -U flash:w:main.hex:i

fuse:
	$(AVRDUDE) $(FUSES)

install: flash fuse

clean:
	rm -f main.hex main.elf $(OBJECTS)

main.elf: $(OBJECTS)
	$(COMPILE) -o main.elf $(OBJECTS)

main.hex: main.elf
	rm -f main.hex
	avr-objcopy -j .text -j .rodata -j .data -O ihex main.elf main.hex
	avr-size -C --mcu=$(DEVICE) main.elf

disasm:	main.elf
	avr-objdump -d main.elf

cpp:
	$(COMPILE) -E main.c
